The main idea of this script is to run it inside an directory (for exaple 0001) with some content for backup.<br>
This script will:<br>
 1. Create some scripts so you can easyly check and restore the archived content (inside 0001)<br>
 2. Create file with MD5 sums for the original content (inside 0001)<br>
 3. Archive the data (inside 0001) with RAR (nonfree) and add 10% recovery record<br>
 4. Create file with MD5 sums (inside 0001) for the RAR archives, so you can check them with md5sum -c rar.md5<br>
 5. Create a directory (inside 0001) with the same name (you know it 0001) as the main directory and move the data that will be burned on the CD/DVD/BD<br>
 6. Unmount the optical drive if it is mounted by udev<br>
 7. Burn ISO9660 image on the optical media with the same label as the main directory (0001 is dynamic, check "${PWD##*/}")<br>
 8. Don`t eject the drive after the burning ends, mount it with udev, check the archived data, return to the main directory, report for errors and eject the CD.<br>

You are free to modify the code and help me to improve it.<br>
I know that rar isn't free, but i dont want to deal with tar,p7zip and par2 simontaniously. Also i want to make a good use of my rar license.<br>
